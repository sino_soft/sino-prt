package com.sinosoft.controller.test;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by imbob on 2017/6/10.
 */
@RestController
public class HelloWorld {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @ApiOperation(value="helloWorld", notes="这是一个测试")
    @RequestMapping("/helloworld")
    public List helloworld(){

        StringBuffer sql = new StringBuffer("SELECT * from BASE_USER r WHERE r.USER_ID = 10011");

        List  list = jdbcTemplate.queryForList(sql.toString());
        return list;
    }



}
