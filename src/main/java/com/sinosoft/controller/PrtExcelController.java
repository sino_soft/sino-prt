package com.sinosoft.controller;

import com.sinosoft.domain.BaseSubScriptionMod;
import com.sinosoft.domain.Result;
import com.sinosoft.service.PrtExcelService;
import com.sinosoft.utils.ResultUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <descption></descption>
 * @date ：2017/6/14 14:15
 * @author：梅海波<meihaibo13186@sinosoft.com.cn>
 * @version:0.1
 */
@RestController
public class PrtExcelController {

    @Autowired
    private PrtExcelService prtExcelService;

    @ApiOperation(value="创建SQL模板", notes="创建SQL模板")
    @ApiImplicitParam(name = "baseSubScriptionMod", value = "baseSubScriptionMod", required = true, dataType = "BaseSubScriptionMod")
    @RequestMapping("/creatSubModelquerySql")
    public Result creatSubModelquerySql(@RequestBody BaseSubScriptionMod baseSubScriptionMod){
        System.out.print(baseSubScriptionMod);
        //保存模板文件
        prtExcelService.saveBaseSubScriptionMod(baseSubScriptionMod);



       return ResultUtil.success();
    }

    @ApiOperation(value = "生成excle",notes="这是notes")
    @ApiImplicitParam(name = "baseSubScriptionMod", value = "baseSubScriptionMod", required = true, dataType = "BaseSubScriptionMod")
    @RequestMapping("/querySubModel")
    public Result querySubModel(@RequestBody BaseSubScriptionMod baseSubScriptionMod){

        prtExcelService.printExcel(baseSubScriptionMod);

        return ResultUtil.success();
    }



}
