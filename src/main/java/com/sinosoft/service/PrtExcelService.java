package com.sinosoft.service;

import com.sinosoft.domain.BaseSubScriptionMod;
import com.sinosoft.exception.PrtException;

/**
 * <descption></descption>
 * @date ：2017/6/16 15:49
 * @author：梅海波<meihaibo13186@sinosoft.com.cn>
 * @version:0.1
 */
public interface PrtExcelService {
    void saveBaseSubScriptionMod(BaseSubScriptionMod baseSubScriptionMod)throws PrtException;

    void printExcel(BaseSubScriptionMod baseSubScriptionMod)throws PrtException;
}
