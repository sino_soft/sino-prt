package com.sinosoft.service;

import com.sinosoft.dao.PrtExcelDAO;
import com.sinosoft.domain.BaseSubScriptionMod;
import com.sinosoft.exception.PrtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <descption></descption>
 * @date ：2017/6/16 15:49
 * @author：梅海波<meihaibo13186@sinosoft.com.cn>
 * @version:0.1
 */
@Service("prtExcelService")
@Transactional
public class PrtExcelServiceImpl implements PrtExcelService{

    @Autowired
    private PrtExcelDAO prtExcelDAO;

    @Override
    public void saveBaseSubScriptionMod(BaseSubScriptionMod baseSubScriptionMod) throws PrtException {
        prtExcelDAO.saveBaseSubScriptionMod(baseSubScriptionMod);
    }

    @Override
    public void printExcel(BaseSubScriptionMod baseSubScriptionMod) throws PrtException {
        List list = prtExcelDAO.printExcel(baseSubScriptionMod);
        if(null != list && list.size()!=0){
            Map<Object,Object> map = (Map) list.get(0);
            for (Map.Entry<Object,Object> entry : map.entrySet()) {
                System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

            }
        }



    }
}
