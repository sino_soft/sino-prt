package com.sinosoft.dao;

import com.sinosoft.domain.BaseSubScriptionMod;
import com.sinosoft.exception.PrtException;

import java.util.List;

/**
 * <descption></descption>
 * @date ：2017/6/16 15:50
 * @author：梅海波<meihaibo13186@sinosoft.com.cn>
 * @version:0.1
 */
public interface PrtExcelDAO {
    void saveBaseSubScriptionMod(BaseSubScriptionMod baseSubScriptionMod)throws PrtException;

    List printExcel(BaseSubScriptionMod baseSubScriptionMod)throws PrtException;
}
