package com.sinosoft.dao;

import com.sinosoft.domain.BaseSubScriptionMod;
import com.sinosoft.exception.PrtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <descption></descption>
 * @date ：2017/6/16 15:50
 * @author：梅海波<meihaibo13186@sinosoft.com.cn>
 * @version:0.1
 */
@Repository("prtExcelDAO")
public class PrtExcelDAOImpl implements PrtExcelDAO{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void saveBaseSubScriptionMod(BaseSubScriptionMod baseSubScriptionMod) throws PrtException {

    }

    @Override
    public List printExcel(BaseSubScriptionMod baseSubScriptionMod) throws PrtException {
        List list = jdbcTemplate.queryForList(baseSubScriptionMod.getSubModSql());


        return list;

    }
}
