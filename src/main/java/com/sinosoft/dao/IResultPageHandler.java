package com.sinosoft.dao;

import java.util.List;

/**
 * <descption></descption>
 * @date ：2017/6/14 14:27
 * @author：梅海波<meihaibo13186@sinosoft.com.cn>
 * @version:0.1
 */
public interface IResultPageHandler {
    /**
     *
     * @param headAndDataList 第一元素是表头list，第二元素是数据list。
     * @throws Exception
     * @方法说明 被数据DAO回调，用于处理已获得的数据，例如将数据写入excel文件等。
     * @date 2015-2-6
     * @author 范强
     */
    void handleResultPage(List headAndDataList) throws RuntimeException;

    /**
     *
     * @param headAndDataList 第一元素是表头list，第二元素是数据list。
     * @throws Exception
     * @方法说明 被数据DAO回调，用于处理已获得的数据，例如将数据写入excel文件等。（账单专用）
     * @date 2017-3-6
     * @author 刘小军
     */
    void handleResultPages(List headAndDataList) throws RuntimeException;

    /**
     *
     * @throws Exception
     * @方法说明 数据处理完毕后回调触发清理工作。
     * @date 2015-2-10
     * @author 范强
     */
    void close() throws RuntimeException;

    void handleResult(List headAndDataList,int manyColumn) throws RuntimeException;

    void changeSheet(String sheetName) throws RuntimeException;
}
