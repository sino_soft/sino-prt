package com.sinosoft.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * <descption></descption>
 * @date ：2017/6/16 12:03
 * @author：梅海波<meihaibo13186@sinosoft.com.cn>
 * @param
 * @return
 * @throws
 */
@Data
@AllArgsConstructor
public class BaseSubScriptionMod {
    private Long subModId;
    private String subModTitle;
    private Date date;
    private Long createId;
    private Integer sqlType;
    private String sqlDesc;
    private Integer modState;
    private Long asscAction;
    private String subModSql;

}
