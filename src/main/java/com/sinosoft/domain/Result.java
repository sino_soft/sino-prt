package com.sinosoft.domain;

/**
 * <descption></descption>
 * @date ：2017/6/10 15:16
 * @author：梅海波<meihaibo13186@sinosoft.com.cn>
 * @version:0.1
 */
import lombok.Data;

/**
 * <descption></descption>
 * @date ：2017/6/10 15:12
 * @author：梅海波<meihaibo13186@sinosoft.com.cn>
 * @version:0.1
 */
@Data
public class Result<T> {
    private Integer code;

    private String msg;

    private T data;
}
