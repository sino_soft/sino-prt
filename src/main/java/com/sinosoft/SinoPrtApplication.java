package com.sinosoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SinoPrtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SinoPrtApplication.class, args);
	}
}
