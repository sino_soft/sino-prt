package com.sinosoft.aspect;

import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by imbob on 2017/6/10.
 */
@Aspect
@Component
public class HttpAspect {
    private final static Logger LOGGER = LoggerFactory.getLogger(HttpAspect.class);


    @Pointcut("execution(* com.sinosoft.controller..*.*(..))")
    public void log(){
        System.out.println(11111);
    }

    @Before("log()")
    public void doBefore(){
        System.out.println(22222);
    }

    @After("log()")
    public void doAfter(){
        System.out.println(33333);
    }

    @AfterReturning(returning = "object",pointcut = "log()")
    public void doAfterReturning(Object object){
        LOGGER.info("responts = {}",object.toString());
    }
}
