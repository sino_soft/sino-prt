package com.sinosoft;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * 创建时间：2017/5/8  03:37
 * 类名：GetuniquenoApplicationTests
 * 描述：所有测试的父类
 *
 * @author ： zhangzheng <zhang_zheng@sinsoft.com.cn>
 * @version : v1.1
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {


    //Controller测试
    protected MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationConnect;

    @Before
    public void setUp() throws JsonProcessingException {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationConnect).build();

    }

    @Test
    @Rollback
    public void testHelloworld()throws Exception{
        RequestBuilder request = null;
        request = post("/helloworld");
        mvc.perform(request)
                .andExpect(status().isOk()).andDo(print());
    }




}